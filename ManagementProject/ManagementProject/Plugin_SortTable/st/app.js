var
    process = document.getElementById('process-mid')
    todo = document.getElementById('todo-left'),
    review = document.getElementById('review-mid'),
    done = document.getElementById('done-right');
    

//// Example 1 - Simple list

// Example 2 - Shared lists
new Sortable(todo, {
    group: 'shared', // set both lists to same group
    animation: 150,
    onEnd: function (event) {
        console.log(event);
        var parentTo = $(event.to);
        var idItemTo = $(event.item).attr('id');
        console.log($(event.item));
        var idparentTo = parentTo.attr('id');
        var idparentFrom = $(event.from).attr('id');
        var parameter = {
            statusFrom: idparentFrom,
            statusTo: idparentTo,
            idIssue: idItemTo
        };

        if (idparentFrom != idparentTo) {
            $.ajax({
                type: "POST",
                url: "/Board/UpdateStatus",
                data: parameter,
                dataType: "json",
                success: function (response) {

                }
            });
            if (idparentTo == "done-right") {
                var item = $(event.item);
                item.find("div div div p strong").css("text-decoration", "line-through");
            }
        }

    }

});

new Sortable(done, {
    group: 'shared',
    animation: 150,
    onEnd: function (event) {
        console.log(event);
        var parentTo = $(event.to);
        var idItemTo = $(event.item).attr('id');
        var idparentTo = parentTo.attr('id');
        var idparentFrom = $(event.from).attr('id');
        var parameter = {
            statusFrom: idparentFrom,
            statusTo: idparentTo,
            idIssue: idItemTo
        };

        if (idparentFrom != idparentTo) {
            $.ajax({
                type: "POST",
                url: "/Board/UpdateStatus",
                data: parameter,
                dataType: "json",
                success: function (response) {

                }
            });
            $(event.item).find("div div div p strong").css("text-decoration", "");
        }

    }
});
new Sortable(process, {
    group: 'shared',
    animation: 150,
    onEnd: function (event) {
        console.log(event);
        var parentTo = $(event.to);
        var idItemTo = $(event.item).attr('id');
        var idparentTo = parentTo.attr('id');
        var idparentFrom = $(event.from).attr('id');
        var parameter = {
            statusFrom: idparentFrom,
            statusTo: idparentTo,
            idIssue: idItemTo
        };
        if (idparentFrom != idparentTo) {
            $.ajax({
                type: "POST",
                url: "/Board/UpdateStatus",
                data: parameter,
                dataType: "json",
                success: function (response) {

                }
            });
            if (idparentTo == "done-right") {
                var item = $(event.item);
                item.find("div div div p strong").css("text-decoration", "line-through");
            }
        }
    }
});
new Sortable(review, {
    group: 'shared',
    animation: 150,
    onEnd: function (event) {
        console.log(event);
        var parentTo = $(event.to);
        var idItemTo = $(event.item).attr('id');
        var idparentTo = parentTo.attr('id');
        var idparentFrom = $(event.from).attr('id');
        var parameter = {
            statusFrom: idparentFrom,
            statusTo: idparentTo,
            idIssue: idItemTo
        };

        if (idparentFrom != idparentTo) {
            $.ajax({
                type: "POST",
                url: "/Board/UpdateStatus",
                data: parameter,
                dataType: "json",
                success: function (response) {

                }
            });
            if (idparentTo == "done-right") {
                var item = $(event.item);
                item.find("div div div p strong").css("text-decoration", "line-through");
            }
        }
    }
});

///
new Sortable(userJoin, {
    group: 'user',
    animation: 150,
    onEnd: function (event) {
        console.log(event);
        //var parentTo = $(event.to);
        //var idItemTo = $(event.item).attr('id');
        //var idparentTo = parentTo.attr('id');
        //var idparentFrom = $(event.from).attr('id');
        //var parameter = {
        //    statusFrom: idparentFrom,
        //    statusTo: idparentTo,
        //    idIssue: idItemTo
        //};

        //if (idparentFrom != idparentTo) {
        //    $.ajax({
        //        type: "POST",
        //        url: "/Board/UpdateStatus",
        //        data: parameter,
        //        dataType: "json",
        //        success: function (response) {

        //        }
        //    });
        //    if (idparentTo == "done-right") {
        //        var item = $(event.item);
        //        item.find("div div div p strong").css("text-decoration", "line-through");
        //    }
        //}
    }
});
new Sortable(userNew, {
    group: 'user',
    animation: 150,
    onEnd: function (event) {
        console.log(event);
        //var parentTo = $(event.to);
        //var idItemTo = $(event.item).attr('id');
        //var idparentTo = parentTo.attr('id');
        //var idparentFrom = $(event.from).attr('id');
        //var parameter = {
        //    statusFrom: idparentFrom,
        //    statusTo: idparentTo,
        //    idIssue: idItemTo
        //};

        //if (idparentFrom != idparentTo) {
        //    $.ajax({
        //        type: "POST",
        //        url: "/Board/UpdateStatus",
        //        data: parameter,
        //        dataType: "json",
        //        success: function (response) {

        //        }
        //    });
        //    if (idparentTo == "done-right") {
        //        var item = $(event.item);
        //        item.find("div div div p strong").css("text-decoration", "line-through");
        //    }
        //}
    }
});


