﻿using Management.Service;
using Management.Service.ViewModel;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ManagementProject.Controllers
{
    [Authorize]
    public class CommentController : Controller
    {
        private readonly ICommentService _commentService;
        public CommentController(ICommentService commentService)
        {
            _commentService = commentService;
        }
        // GET: Comment
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost, ValidateInput(false)]
        public async Task<JsonResult> UpsertComment(CommentIssueViewModel model)
        {
            model.UserId = User.Identity.GetUserId();
            if (model.Id == 0)
            {
                var result = await _commentService.CreateCommentAsync(model);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                await _commentService.UpdateAsync(model);
            }
            return Json(new ResultModel { Issuccess = true, Result = "update comment successfully!" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<JsonResult> DeleteComment(int id)
        {
            await _commentService.DeleteAsync(id);

            return Json("success", JsonRequestBehavior.AllowGet);
        }
    }
}