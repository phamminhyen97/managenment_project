﻿using Management.Common;
using Management.Service;
using Management.Service.ViewModel;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ManagementProject.Controllers
{
    [Authorize]
    public class BoardController : Controller
    {
        // GET: Board

        private readonly IProjectService _projectService;

        private readonly IUserService _userService;

        private readonly IModuleService _moduleService;
        public BoardController(IProjectService projectService, IUserService userService, IModuleService moduleService)
        {
            _projectService = projectService;
            _userService = userService;
            _moduleService = moduleService;
        }
        public async Task<ActionResult> Index(int projectId, string userId = null)
        {
            var projects = await _projectService.GetAll();
            ViewBag.IdProject = projectId;
            ViewBag.Projects = new SelectList(projects, "Id", "Name");
            var users = _userService.GetUsers(projectId).ToList();
            ViewBag.Users = users;
            ViewBag.Reporter = new SelectList(users, "Id", "UserName");
            ViewBag.Assignees = new SelectList(users, "Id", "UserName");
            var modules = await _moduleService.GetAll();
            ViewBag.ModuleParent = new SelectList(modules, "Id", "Name");
            var moduleActive = await _moduleService.GetAll(projectId, userId);
            ViewBag.Modules = moduleActive.Where(x => x.Status != (int)EnumConstant.IssueStatus.Close);
            return View(new ModuleViewModel());
        }

        [Authorize]
        public async Task<ActionResult> Test()
        {
            var projects = await _projectService.GetAll();
            ViewBag.Projects = new SelectList(projects, "Id", "Name");
            var users = _userService.GetUsers(null).ToList();
            ViewBag.Reporter = new SelectList(users, "Id", "UserName");
            ViewBag.Assignees = new SelectList(users, "Id", "UserName");
            var modules = await _moduleService.GetAll();
            ViewBag.ModuleParent = new SelectList(modules, "Id", "Name");
            ViewBag.Modules = await _moduleService.GetAll();
            return View(new ModuleViewModel());
        }

        [HttpPost]
        public async Task<ActionResult> Insert(ModuleViewModel model)
        {
            try
            {
                model.CreateBy = User.Identity.GetUserId();
                model.CreateName = User.Identity.GetUserName();
                await _moduleService.CreateAsync(model);
                var projects = await _projectService.GetAll();
                ViewBag.Projects = new SelectList(projects, "Id", "Name");
                var users = _userService.GetUsers(model.IdProject).ToList();
                ViewBag.Reporter = new SelectList(users, "Id", "UserName");
                ViewBag.Assignees = new SelectList(users, "Id", "UserName");
                var modules = await _moduleService.GetAll();
                ViewBag.ModuleParent = new SelectList(modules, "Id", "Name");
                ViewBag.Modules = await _moduleService.GetAll();
                return View("Test", new ModuleViewModel());
            }
            catch (Exception)
            {

                return View();
            }

        }

        [HttpPost]
        public async Task<JsonResult> UpdateStatus(string statusFrom, string statusTo, int idIssue)
        {
            try
            {
                var userId = User.Identity.GetUserId();
                var username = User.Identity.GetUserName();

                var result = await _moduleService.UpdateStatus(statusFrom, statusTo, idIssue, username, userId);

                return Json(result);


                //var projects = await _projectService.GetAll();
                //ViewBag.Projects = new SelectList(projects, "Id", "Name");
                //var users = _userService.GetUsers().ToList();
                //ViewBag.Reporter = new SelectList(users, "Id", "UserName");
                //ViewBag.Assignees = new SelectList(users, "Id", "UserName");
                //var modules = await _moduleService.GetAll();
                //ViewBag.ModuleParent = new SelectList(modules, "Id", "Name");
                //ViewBag.Modules = await _moduleService.GetAll();
                //return View("Test", new ModuleViewModel());
            }
            catch (Exception ex)
            {
                return Json(new ResultModel { Issuccess = false, Result = $"Update Status Issue failed by error[{ex.Message}]!" });
            }

        }
    }
}