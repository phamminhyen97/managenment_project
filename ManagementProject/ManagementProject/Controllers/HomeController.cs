﻿using log4net;
using Management.Service;
using Management.Service.ViewModel;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ManagementProject.Controllers
{
    public class HomeController : Controller
    {
        static readonly ILog log =LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly IProjectService _projectService;
        private readonly INotificationService _notificationService;
        public HomeController(IProjectService projectService, INotificationService notificationService)
        {
            _projectService = projectService;
            _notificationService = notificationService;
        }

        public ActionResult Index()
        {
            log.Info("Init log4net");
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [ChildActionOnly]
        public ActionResult ListProject()
        {
            var model = _projectService.GetAll().Result;
            return PartialView(model);
        }

        [ChildActionOnly]
        public ActionResult ListNotification()
        {
            var user = User.Identity.GetUserId();
            if (!string.IsNullOrEmpty(user))
            {
                var model = _notificationService.GetNotificationByFilter(true, false, user).Result.ToList();
                return PartialView(model);
            }
            return PartialView(new List<NotificationViewModel>());
            
        }
    }
}