﻿using Management.Service;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ManagementProject.Controllers
{
    [Authorize]
    public class NotificationController : Controller
    {
        // GET: Notification
        private readonly INotificationService _notificationService;
        public NotificationController(INotificationService notificationService)
        {
            _notificationService = notificationService;
        }
        public async Task<ActionResult>  Index()
        {
            var model =await _notificationService.GetNotificationByFilter(false, false, User.Identity.GetUserId());
            return View(model);
        }
    }
}