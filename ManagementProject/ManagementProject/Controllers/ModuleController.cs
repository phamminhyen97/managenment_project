﻿using Management.Service;
using Management.Service.ViewModel;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ManagementProject.Controllers
{
    [Authorize]
    public class ModuleController : Controller
    {

        private readonly IModuleService _moduleService;
        private readonly IUserService _userService;
        private ApplicationUserManager _userManager;
        private readonly IProjectService _projectService;
        private readonly ICommentService _commentService;
        public ModuleController(IModuleService moduleService, IUserService userService, ApplicationUserManager userManager,
            IProjectService projectService, ICommentService commentService)
        {
            _moduleService = moduleService;
            _userService = userService;
            UserManager = userManager;
            _projectService = projectService;
            _commentService = commentService;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        // GET: Module
        public ActionResult Index()
        {
            return View();
        }


        public async Task<ActionResult> Create(int projectId)
        {
            var projects = await _projectService.GetAll(projectId);
            ViewBag.Projects = new SelectList(projects, "Id", "Name");
            var users = _userService.GetUsers(projectId).ToList();
            ViewBag.Users = users;
            ViewBag.Reporter = new SelectList(users, "Id", "UserName");
            ViewBag.Assignees = new SelectList(users, "Id", "UserName");
            var modules = await _moduleService.GetAll();
            ViewBag.ModuleParent = new SelectList(modules, "Id", "Name");
            return View(new ModuleViewModel());
        }

        [HttpPost]
        public async Task<JsonResult> CreateSubTask(SubTaskViewModel model)
        {
            model.UserId = User.Identity.GetUserId();
            var result = await _moduleService.CreateSubTask(model);
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public async Task<ActionResult> Create(ModuleViewModel model)
        {
            try
            {
                model.CreateBy = User.Identity.GetUserId();
                model.CreateName = User.Identity.GetUserName();
                await _moduleService.CreateAsync(model);
                var projects = await _projectService.GetAll();
                ViewBag.Projects = new SelectList(projects, "Id", "Name");
                var users = _userService.GetUsers(model.IdProject).ToList();
                ViewBag.Reporter = new SelectList(users, "Id", "UserName");
                ViewBag.Assignees = new SelectList(users, "Id", "UserName");
                var modules = await _moduleService.GetAll();
                ViewBag.ModuleParent = new SelectList(modules, "Id", "Name");
                ViewBag.Modules = await _moduleService.GetAll();
                return RedirectToAction("Index", "Board", new { projectId = model.IdProject, userId = User.Identity.GetUserId() });
            }
            catch (Exception)
            {
                var projects = await _projectService.GetAll(model.IdProject);
                ViewBag.Projects = new SelectList(projects, "Id", "Name");
                var users = _userService.GetUsers(model.IdProject).ToList();
                ViewBag.Users = users;
                ViewBag.Reporter = new SelectList(users, "Id", "UserName");
                ViewBag.Assignees = new SelectList(users, "Id", "UserName");
                var modules = await _moduleService.GetAll();
                ViewBag.ModuleParent = new SelectList(modules, "Id", "Name");
                return View(model);
            }

        }
        public async Task<ActionResult> Detail(int? id)
        {

            var user =await _userService.FindByIdAsync(User.Identity.GetUserId());
            ViewBag.User = user;

            ViewBag.Comments = await _commentService.GetCommentOfIssue(id.Value);
            var model = await _moduleService.FindByIdAsync(id);
            ViewBag.Users = _userService.GetUsers(model.IdProject);
            if (model != null)
            {
                return View(model);
            }
            return RedirectToAction("Index", "Home");
        }

        [HttpPost, ValidateInput(false)]
        public async Task<JsonResult> UpdateModule(ModuleUpdateViewModel model)
        {
            try
            {
                model.UserId = User.Identity.GetUserId();
                var user = UserManager.FindById(model.UserId);
                model.UserName = user.Name ?? user.UserName;
                var result = await _moduleService.UpdateDetail(model);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new ResultModel { Issuccess = false, Result = $"Error :[{ex.Message}]" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public async Task<JsonResult> GetModuleJson(int id)
        {
            try
            {
                var model = await _moduleService.GetAll(projectId: id);
                var htmlString = RenderPartialViewToString("_ListModule", model);
                return Json(htmlString, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public async Task<ActionResult> ModuleDetail(int id)
        {
            try
            {
                var model = await _moduleService.GetAll(projectId: id);
                return View(model);
            }
            catch (Exception ex)
            {
                return View();
            }
        }

        protected string RenderPartialViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");

            ViewData.Model = model;

            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult =
                ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext viewContext = new ViewContext
                (ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                return sw.GetStringBuilder().ToString();
            }
        }
    }
}