﻿using Management.Service;
using Management.Service.ViewModel;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ManagementProject.Controllers
{
    [Authorize]
    public class ProjectController : Controller
    {
        // GET: Project
        private readonly IProjectService _projectService;
        private IUserService _userService;
        public ProjectController(IProjectService projectService, IUserService userService)
        {
            _projectService = projectService;
            _userService = userService;
        }
        public async Task<ActionResult> Index()
        {
            var userRole = User.IsInRole("Admin");
            if (userRole)
            {
                var model = await _projectService.GetAll();
                return View(model);
            }
            else
            {
                var model = await _projectService.GetAll(userId: User.Identity.GetUserId());
                return View(model);
            }
        }

        public ActionResult Create()
        {
            ViewBag.Users = _userService.GetUsers(null);

            return View(new ProjectViewModel());
        }

        public async Task<ActionResult> Edit(int id)
        {
            ViewBag.Users = _userService.GetUsers(null);
            var model =await _projectService.FindByIdAsync(id);
            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(ProjectViewModel project, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                var url = string.Empty;
                var fileName = string.Empty;
                string _path = string.Empty;
                if (file?.ContentLength > 0)
                {
                    url = "~/Upload/project";
                    fileName = DateTime.Now.Ticks + ".png";
                    _path = Path.Combine(Server.MapPath(url), fileName);
                    file.SaveAs(_path);
                    if (fileName.Length>0)
                    {
                        project.Avatar = url + "/" + fileName;
                    }
                }

                project.UpdateDate = DateTime.Now;
                await _projectService.UpdateAsync(project);
                return RedirectToAction("Index", "Project");
            }
            ViewBag.Users = _userService.GetUsers(null);
            var model = await _projectService.FindByIdAsync(project.Id);
            return View(model);
        }
        // POST: Admin/Project/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(ProjectViewModel project, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                var url = string.Empty;
                var fileName = string.Empty;
                string _path = string.Empty;
                if (file.ContentLength > 0)
                {
                    url = "~/Upload/project";
                    fileName = DateTime.Now.Ticks + ".png";
                    _path = Path.Combine(Server.MapPath(url), fileName);
                    file.SaveAs(_path);
                    project.Avatar = url + "/" + fileName;
                }

                project.CreateDay = DateTime.Now;
                await _projectService.CreateAsync(project);
                return RedirectToAction("Index", "Project");
            }
            return View(project);
        }


        [HttpPost]
        public async Task<JsonResult> UpdateLeader(int idProject, string userId)
        {
            try
            {
                var result = await _projectService.UpdateLeader(idProject, userId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new ResultModel { Issuccess = false, Result = $"Error :[{ex.Message}]" }, JsonRequestBehavior.AllowGet);
            }
        }

        public async Task<ActionResult> AddUser(int projectId)
        {
            var users = _userService.GetUsers(null).ToList();
            var userJoin = users.Where(x => x.UserJoinProjects.Where(q => q.IdProject == projectId).Any()).ToList();
            var userNew = users.Except(userJoin).ToList();
            ViewBag.userJoins = userJoin;
            ViewBag.userNews = userNew;
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> UpsertUserToProject(int projectId, string userId)
        {
            var result = await _projectService.UpsertUserToProject(projectId, userId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public string GetUserById(string id)
        {
            var user = _userService.FindByIdAsync(id).Result;
            return user.Name ?? user.UserName;

        }
        public string GetUserAvatar(string id)
        {
            var user = _userService.FindByIdAsync(id).Result;
            return user.Avatar ?? string.Empty;

        }
    }
}