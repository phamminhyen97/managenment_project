﻿using Management.Service;
using Management.Service.ViewModel;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ManagementProject.Controllers
{
    [Authorize]
    public class WorkLogController : Controller
    {
        private readonly IWorkLogService _workLogService;

        private readonly IModuleService _moduleService;
        public WorkLogController(IWorkLogService workLogService, IModuleService moduleService)
        {
            _workLogService = workLogService;
            _moduleService = moduleService;
        }

        // GET: WorkLog
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> AddWorkLog(WorkLogViewModel model)
        {
            await _workLogService.CreateAsync(model);

            return Json("ok", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<ActionResult> AddWorkLogControl(WorkLogViewModel model)
        {
            model.CreateDate = DateTime.Now;
            model.UpdateDate = DateTime.Now;
            model.AuthorId = User.Identity.GetUserId();
            await _workLogService.CreateAsync(model);
            return RedirectToAction("Detail", "Module", new { id = model.ModuleId });
        }

        [HttpGet]
        public async Task<ActionResult> AddLogWork(int? IdModule)
        {
            var issues = new List<ModuleViewModel>();
            if (IdModule != null)
            {
                var reuslt = await _moduleService.FindByIdAsync(IdModule);
                issues.Add(reuslt);
            }
            else
            {
                var moduleViews = await _moduleService.GetModuleByFilter(User.Identity.GetUserId());
                issues = moduleViews.ToList();
            }
            return View(issues);
        }
    }
}