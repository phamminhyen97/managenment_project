using log4net;
using Management.Entity;
using Management.Service;
using Management.Service.Implement;
using ManagementProject.Controllers;
using ManagementProject.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using System.Web.Mvc;
using Unity;
using Unity.Injection;
using Unity.Lifetime;
using Unity.Mvc5;

namespace ManagementProject
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
            var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();

            //container.AddNewExtension<ILog>(new InjectionFactory(x => new LogManager.GetLogger(typeof(this))));

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
            container.RegisterType<DbContext, ManagementDB>(new HierarchicalLifetimeManager());
            container.RegisterType<UserManager<ApplicationUser>>(new HierarchicalLifetimeManager());
            container.RegisterType<IUserStore<ApplicationUser>, UserStore<ApplicationUser>>(new HierarchicalLifetimeManager());
            container.RegisterType<AccountController>(new InjectionConstructor());
            container.RegisterType<ManageController>(new InjectionConstructor());

            container.RegisterType<IProjectService,ProjectService>();
            container.RegisterType<IUserService, UserService>();
            container.RegisterType<IModuleService, ModuleService>();
            container.RegisterType<ICommentService, CommentService>();
            container.RegisterType<IWorkLogService, WorkLogService>();
            container.RegisterType<INotificationService, NotificationService>();
            //container.RegisterType<ILog>(new InjectionFactory(factory => LogManager.GetLogger()));

        }
    }
}