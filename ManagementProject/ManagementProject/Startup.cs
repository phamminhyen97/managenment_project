﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ManagementProject.Startup))]
namespace ManagementProject
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
