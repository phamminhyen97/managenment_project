﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Common
{
    public enum LoggingEnum
    {
        [Description("Insert Project successfully by Id [{0}]!")]
        SS0001,
        [Description("Update Project successfully by Id [{0}] !")]
        SS0002,
        [Description("Delete Project successfully by Id [{0}]!")]
        SS0003,

        [Description("Insert Project failed by Id [{0}]!. Because error [{1}]")]
        FL0001,
        [Description("Update Project failed by Id [{0}]!. Because error [{1}]")]
        FL0002,
        [Description("Delete Project failed by Id [{0}]!. Because error [{1}]")]
        FL0003,

        [Description("Insert Module successfully by Id [{0}]!")]
        SS0011,
        [Description("Update Module successfully by Id [{0}]!")]
        SS0012,
        [Description("Delete Module successfully by Id [{0}]!")]
        SS0013,
        [Description("Insert Project failed by Id [{0}]!. Because error [{1}]")]
        FL0011,
        [Description("Update Project failed by Id [{0}]!. Because error [{1}]")]
        FL0012,
        [Description("Delete Project failed by Id [{0}]!. Because error [{1}]")]
        FL0013,

        [Description("Insert User successfully by Id [{0}]!")]
        SS0021,
        [Description("Update User successfully by Id [{0}]!")]
        SS0022,
        [Description("Delete User successfully by Id [{0}]!")]
        SS0023,
        [Description("Insert User failed by Id [{0}]!. Because error [{1}]")]
        FL0021,
        [Description("Update User failed by Id [{0}]!. Because error [{1}]")]
        FL0022,
        [Description("Delete User failed by Id [{0}]!. Because error [{1}]")]
        FLL0023,

        [Description("Insert HistoryModule successfully by Id [{0}]!")]
        SS0031,
        [Description("Update HistoryModule successfully by Id [{0}]!")]
        SS0032,
        [Description("Delete HistoryModule successfully by Id [{0}]!")]
        SS0033,
        [Description("Insert HistoryModule failed by Id [{0}]!. Because error [{1}]")]
        FL0031,
        [Description("Update HistoryModule failed by Id [{0}]!. Because error [{1}]")]
        FL0032,
        [Description("Delete HistoryModule failed by Id [{0}]!. Because error [{1}]")]
        FLL0033,

        [Description("Insert ProjectRole successfully by Id [{0}]!")]
        SS0041,
        [Description("Update ProjectRole successfully by Id [{0}]!")]
        SS0042,
        [Description("Delete ProjectRole successfully by Id [{0}]!")]
        SS0043,
        [Description("Insert ProjectRole failed by Id [{0}]!. Because error [{1}]")]
        FL0041,
        [Description("Update ProjectRole failed by Id [{0}]!. Because error [{1}]")]
        FL0042,
        [Description("Delete ProjectRole failed by Id [{0}]!. Because error [{1}]")]
        FL0043,


    }
}
