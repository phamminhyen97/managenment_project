﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Management.Common
{
    public static class Extensions
    {
        public static string GetDescription(this Enum e)
        {
            var attribute =
                e.GetType()
                    .GetTypeInfo()
                    .GetMember(e.ToString())
                    .FirstOrDefault(member => member.MemberType == MemberTypes.Field)
                    .GetCustomAttributes(typeof(DescriptionAttribute), false)
                    .SingleOrDefault()
                    as DescriptionAttribute;

            return attribute?.Description ?? e.ToString();
        }

        public static string GetValue(int id, string type)
        {
            switch (type)
            {
                case "issue":
                    switch (id)
                    {
                        case 1:
                            return "Improvement";
                        case 2:
                            return "Task";
                        case 3:
                            return "NewFeature";
                        case 4:
                            return "Bug";
                        case 5:
                            return "Subtask";
                        default:
                            return "Subtask";
                    }
                case "priority":
                    switch (id)
                    {
                        case 1:
                            return "Highest";
                        case 2:
                            return "High";
                        case 3:
                            return "Medium";
                        case 4:
                            return "Low";
                        case 5:
                            return "Lowest";
                        default:
                            return "Medium";
                    }
                case "status":
                    switch (id)
                    {
                        case 1:
                            return "Todo";
                        case 2:
                            return "Progress";
                        case 3:
                            return "Review";
                        case 4:
                            return "Done";
                        case 5:
                            return "Close";
                        default:
                            return "Todo";
                    }
            }
            return string.Empty;
        }
    }
}
