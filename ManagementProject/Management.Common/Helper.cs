﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Common
{
    public static class Helper
    {
        public static string GetStatus(string stauts)
        {
            var result = string.Empty;
            switch (stauts)
            {
                case "todo-left":
                    result = "To-do";
                    break;
                case "process-mid":
                    result = "Progress";
                    break;
                case "review-mid":
                    result = "Review";
                    break;
                case "done-right":
                    result = "Done";
                    break;
                case "close":
                    result = "Close";
                    break;
                default:
                    result = "Close";
                    break;
            }
            return result;
        }
    }
}
