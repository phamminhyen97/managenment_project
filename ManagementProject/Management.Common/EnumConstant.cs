﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Common
{
    public class EnumConstant
    {

        public enum RoleProject
        {
            Lead = 1,
            Tester = 2,
            Develop = 3
        }

        public enum IssueType
        {
            [Description("Improvement")]
            Improvement = 1,
            [Description("Task")]
            Task = 2,
            [Description("New feature")]
            NewFeature = 3,
            [Description("Bug")]
            Bug = 4,
            [Description("Sub task")]
            Subtask = 5
        }
        public enum PriorityType
        {
            [Description("Highest")]
            Highest = 1,
            [Description("High")]
            High = 2,
            [Description("Medium")]
            Medium = 3,
            [Description("Low")]
            Low = 4,
            [Description("Lowest")]
            Lowest = 5
        }

        public enum IssueStatus
        {
            [Description("To Do")]
            Todo = 1,
            [Description("In Progerss")]
            Progress = 2,
            [Description("Review")]
            Review = 3,
            [Description("Done")]
            Done = 4,
            [Description("Close")]
            Close = 5
        }

        public enum History
        {
            Create = 1,
            Update = 2,
            ChangeReporter = 3,
            Remove = 4,
            ChangeStatus = 5,
            Comment = 6,
            CreateSubtask = 7
        }

        public enum NotificationType
        {
            Comment = 1,
            Nofitication = 2,
            Logwork = 3,
            UpdateIssue = 4
        }

        public enum ProjectStatus
        {
            InProgress = 1,
            Done = 2,
            Close = 3,
        }
    }
}
