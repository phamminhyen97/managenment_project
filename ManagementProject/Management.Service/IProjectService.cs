﻿using Management.Entity;
using Management.Service.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Service
{
    public interface IProjectService : IBaseService<Project, ProjectViewModel>
    {
        Task<ResultModel> UpdateLeader(int projectId, string userId);

        Task<ResultModel> UpsertUserToProject(int projectId, string userId);
    }
}
