﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Management.Service.ViewModel
{
   public class ProjectViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        [AllowHtml]
        //[UIHint("_TinyMCEMini")]
        public string Description { get; set; }

        public string IdManager { get; set; }

        public DateTime? StarDay { get; set; }

        public DateTime? FinishDay { get; set; }

        public DateTime? CreateDay { get; set; }

        public DateTime? UpdateDate { get; set; }

        public string Avatar { get; set; }
        public int? Status { get; set; }

        public string IdLead { get; set; }

        public string Leader { get; set; }

        public string AvatarLead { get; set; }

        [Required]
        [MaxLength(4)]
        public string Key { get; set; }

        public virtual List<ModuleViewModel> Modules { get; set; }

        public virtual List<UserJoinProjectViewModel> UserJoinProjects { get; set; }
    }
}
