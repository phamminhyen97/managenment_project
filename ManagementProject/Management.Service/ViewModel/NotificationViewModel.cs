﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Service.ViewModel
{
  public  class NotificationViewModel
    {
        public long Id { get; set; }
        public int TypeId { get; set; }
        public string Description { get; set; }
        public string FromUser { get; set; }
        public string ToUser { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public bool IsSeen { get; set; }
        public bool IsRead { get; set; }
        public int ModuleId { get; set; }
        public virtual Entity.Type Type { get; set; }
        public virtual UserViewModel FromSend { get; set; }
        public virtual UserViewModel SendTo { get; set; }
    }
}
