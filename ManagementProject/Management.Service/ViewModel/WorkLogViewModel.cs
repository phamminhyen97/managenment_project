﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Service.ViewModel
{
    public class WorkLogViewModel
    {
        public int Id { get; set; }
        public string AuthorId { get; set; }
        public int ModuleId { get; set; }
        public string AuthorName { get; set; }
        public string AuthorAvatar { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public int TimeLog { get; set; }
        public string Description { get; set; }
    }
}
