﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Service.ViewModel
{
   public class UserViewModel
    {
        public string Id { get; set; }
        public DateTime? CreateDate { get; set; }

        public bool? Sex { get; set; }

        public DateTime? DateOfBirth { get; set; }

        public string Avatar { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public string UserName { get; set; }


        public  List<UserJoinProjectViewModel> UserJoinProjects { get; set; }
    }
}
