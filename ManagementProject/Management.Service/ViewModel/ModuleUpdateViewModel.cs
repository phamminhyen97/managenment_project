﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations.Builders;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Service.ViewModel
{
    public class ModuleUpdateViewModel
    {
        public int IdModule { get; set; }
        public int? IssueStatus { get; set; }
        public int? IssueType { get; set; }
        public int? PriorityType { get; set; }
        public string AssigneeId { get; set; }
        public string AssigneeFrom { get; set; }
        public string AssigneeTo { get; set; }
        public string ReporterId { get; set; }
        public string ReporterFrom { get; set; }
        public string ReporterTo { get; set; }
        public string Description { get; set; }
        public int? Point { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string ModuleName { get; set; }
    }
}
