﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Service.ViewModel
{
    public class HistoryModuleViewModel
    {
        public long Id { get; set; }

        public int IdModule { get; set; }
        public string AssignedFrom { get; set; }

        public string AssignedFromName { get; set; }
        public string AssignedFromAvatar { get; set; }
        public string AssignedTo { get; set; }
        public string AssignedToName { get; set; }
        public string AssignedToAvatar { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public string Description { get; set; }
        public int Status { get; set; }
        public virtual ModuleViewModel Module { get; set; }
    }
}
