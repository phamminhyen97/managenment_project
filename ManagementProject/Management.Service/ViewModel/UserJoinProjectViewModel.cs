﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Service.ViewModel
{
    public class UserJoinProjectViewModel
    {
        public int Id { get; set; }
        public string IdUser { get; set; }
        public int IdProject { get; set; }
        public DateTime? JoinedDay { get; set; }
        public string Description { get; set; }
        public int? IdRole { get; set; }
        public  ProjectViewModel Project { get; set; }
        public  ProjectRoleViewModel ProjectRole { get; set; }
        public  UserViewModel User { get; set; }
    }
}
