﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Migrations.Builders;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Management.Service.ViewModel
{
    public class ModuleViewModel
    {
        public int Id { get; set; }

        public int? IdProject { get; set; }

        public string Name { get; set; }

        public string Summary { get; set; }

        [AllowHtml]
        [UIHint("_TinyMCEMini")]
        public string Description { get; set; }

        public DateTime? StartDay { get; set; }

        public DateTime? FinishDay { get; set; }

        public int? Point { get; set; }

        public int? IdModuleParent { get; set; }

        public int? Status { get; set; }

        public string UserId { get; set; }

        public string CreateBy { get; set; }

        public int IssueType { get; set; }

        public int PriorityType { get; set; }
        public string Reporter { get; set; }
        public string ReporterName { get; set; }
        public string ReporterAvatar { get; set; }
        public string Assignee { get; set; }
        public string AssigneeName { get; set; }
        public string AssigneeAvatar { get; set; }
        public string CreateName { get; set; }
        public string Avatar { get; set; }
        public int Estimated { get; set; }
        public int Remaining { get; set; }
        public int Logged { get; set; }

        private float intLoggedPer = 0;
        public float LoggedPer
        {
            get
            {

                if (Estimated == 0)
                {
                    return intLoggedPer;
                }
                else
                {
                    intLoggedPer = (((float)Logged) / Estimated) * 100;
                    return intLoggedPer;
                }
            }
            set
            {
                if (Estimated == 0)
                {
                    intLoggedPer = value;
                }
                else
                {
                    intLoggedPer = (((float)Logged) / Estimated) * 100;
                    intLoggedPer = value;
                }
            }
        }
        private float RemainingPerTmp = 0;
        public float RemainingPer
        {
            get {
                if (LoggedPer > 100)
                {
                  return  RemainingPerTmp ;
                }
                else
                {
                    RemainingPerTmp = 100 - LoggedPer;
                    return RemainingPerTmp;
                }
            }
            set
            {
                if (LoggedPer > 100)
                {
                    RemainingPerTmp = value;
                }
                else
                {
                    RemainingPerTmp = 100 - LoggedPer;
                    RemainingPerTmp = value;
                }
            }
        }
        public virtual List<HistoryModuleViewModel> Histories { get; set; }

        public virtual ModuleViewModel ModuleParent { get; set; }

        public virtual ProjectViewModel Project { get; set; }
        public virtual ICollection<ModuleViewModel> Module1 { get; set; }

        public virtual ICollection<WorkLogViewModel> WorkLogs { get; set; }
    }
}
