﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Service.ViewModel
{
  public  class CommentIssueViewModel
    {
        public long Id { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string UserAvatar { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string Description { get; set; }
        public int ModuleId { get; set; }
        public string ModuleName { get; set; }
        public  ModuleViewModel Module { get; set; }
        public  UserViewModel User { get; set; }
    }
}
