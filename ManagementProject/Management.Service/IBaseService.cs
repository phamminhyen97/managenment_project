﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Service
{
    public interface IBaseService<TEntity, TDto>
             where TEntity : class
             where TDto : class
    {
        Task CreateAsync(TDto dto);
        Task<bool> UpdateAsync(TDto dto);
        Task DeleteAsync(object keyValues);
        Task<TDto> FindByIdAsync(object keyValues);
        Task<IEnumerable<TDto>> GetAll(int? projectId = null, string userId = null);
    }
}
