﻿using Management.Entity;
using Management.Service.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Service
{
    public interface IModuleService : IBaseService<Module, ModuleViewModel>
    {
        Task<ResultModel> UpdateStatus(string statusFrom, string statusTo, int id, string username, string userId);
        Task<ResultModel> UpdateDetail(ModuleUpdateViewModel model);
        Task<ResultModel> CreateSubTask(SubTaskViewModel model);

        Task<IEnumerable<ModuleViewModel>> GetModuleByFilter(string userId);
    }
}
