﻿using AutoMapper;
using Management.Common;
using Management.Entity;
using Management.Service.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Management.Common.EnumConstant;

namespace Management.Service.MapperConfig
{
    public static class MapperConfig
    {
        public static Mapper InitializeAutomapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<ProjectRole, ProjectRoleViewModel>();

                cfg.CreateMap<ProjectRoleViewModel, ProjectRole>();

                cfg.CreateMap<Module, ModuleViewModel>()
                .ForMember(dest => dest.ReporterAvatar, opt => opt.MapFrom(src => src.ReporterUser.Avatar))
                .ForMember(dest => dest.ReporterName, opt => opt.MapFrom(src => src.ReporterUser.Name ?? src.ReporterUser.UserName))
                .ForMember(dest => dest.Avatar, opt => opt.MapFrom(src => src.AssignesUser.Avatar))
                .ForMember(dest => dest.AssigneeAvatar, opt => opt.MapFrom(src => src.AssignesUser.Avatar))
                .ForMember(dest => dest.Estimated, opt => opt.MapFrom(src => src.Point.GetValueOrDefault()))
                .ForMember(dest => dest.AssigneeName, opt => opt.MapFrom(src => src.AssignesUser.Name ?? src.AssignesUser.UserName));
                cfg.CreateMap<ModuleViewModel, Module>()
                .ForMember(dest => dest.Status, opt => opt.MapFrom(src => (int)IssueStatus.Todo))
                .ForMember(dest => dest.Assignee, opt => opt.MapFrom(src => src.UserId));

                cfg.CreateMap<UserJoinProject, UserJoinProjectViewModel>();

                cfg.CreateMap<UserJoinProjectViewModel, UserJoinProject>();

                cfg.CreateMap<HistoryModule, HistoryModuleViewModel>()
                .ForMember(dest => dest.AssignedFromName, opt => opt.MapFrom(src => src.FromUser.Name ?? src.FromUser.UserName))
                .ForMember(dest => dest.AssignedFromAvatar, opt => opt.MapFrom(src => src.FromUser.Avatar))
                .ForMember(dest => dest.AssignedToAvatar, opt => opt.MapFrom(src => !string.IsNullOrEmpty(src.AssignedTo) ? src.ToUser.Avatar : string.Empty))
                .ForMember(dest => dest.AssignedToName, opt => opt.MapFrom(src => !string.IsNullOrEmpty(src.AssignedTo) ? src.ToUser.Name : string.Empty));
                cfg.CreateMap<HistoryModuleViewModel, HistoryModule>();

                cfg.CreateMap<ApplicationUser, UserViewModel>();

                cfg.CreateMap<UserViewModel, ApplicationUser>();

                cfg.CreateMap<Project, ProjectViewModel>()
                .ForMember(dest => dest.IdLead, opt => opt.MapFrom(src => src.UserJoinProjects.First(x => x.User.Id == x.IdUser).User.Id))
                .ForMember(dest => dest.Leader, opt => opt.MapFrom(src => src.UserJoinProjects.First(x => x.User.Id == x.IdUser).User.UserName))
                .ForMember(dest => dest.AvatarLead, opt => opt.MapFrom(src => src.UserJoinProjects.First(x => x.User.Id == x.IdUser).User.Avatar));
                cfg.CreateMap<ProjectViewModel, Project>()
                .ForMember(dest => dest.CreateDay, opt => opt.UseDestinationValue())
                .ForMember(dest => dest.UserJoinProjects, opt => opt.MapFrom(src => src.UserJoinProjects));

                cfg.CreateMap<CommentIssueViewModel, CommentIssue>();

                cfg.CreateMap<CommentIssue, CommentIssueViewModel>()
                .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.User.Name ?? src.User.UserName))
                .ForMember(dest => dest.UserAvatar, opt => opt.MapFrom(src => src.User.Avatar));

                cfg.CreateMap<SubTaskViewModel, Module>()
                .ForMember(dest => dest.IdModuleParent, opt => opt.MapFrom(src => src.ModuleId))
                .ForMember(dest => dest.IssueType, opt => opt.MapFrom(src => (int)IssueType.Subtask))
                .ForMember(dest => dest.Status, opt => opt.MapFrom(src => (int)IssueStatus.Todo))
                .ForMember(dest => dest.PriorityType, opt => opt.MapFrom(src => (int)PriorityType.Medium))
                .ForMember(dest => dest.Reporter, opt => opt.MapFrom(src => src.UserId))
                .ForMember(dest => dest.Assignee, opt => opt.MapFrom(src => src.UserId))
                .ForMember(dest => dest.CreateBy, opt => opt.MapFrom(src => src.UserId))
                .ForMember(dest => dest.IdProject, opt => opt.MapFrom(src => src.ProjectId));
                cfg.CreateMap<Module, SubTaskViewModel>();

                cfg.CreateMap<WorkLog, WorkLogViewModel>()
                .ForMember(dest => dest.AuthorName, opt => opt.MapFrom(src => src.Author.Name ?? src.Author.UserName))
                .ForMember(dest => dest.AuthorAvatar, opt => opt.MapFrom(src => src.Author.Avatar));

                cfg.CreateMap<WorkLogViewModel, WorkLog>();
                cfg.CreateMap<Notification, NotificationViewModel>();
                cfg.CreateMap<NotificationViewModel, Notification>();
            });
            return new Mapper(config);
        }
    }
}
