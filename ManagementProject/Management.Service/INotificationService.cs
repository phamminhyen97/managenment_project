﻿using Management.Entity;
using Management.Service.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Service
{
    public interface INotificationService : IBaseService<Notification, NotificationViewModel>
    {
        Task<IEnumerable<NotificationViewModel>> GetNotificationByFilter(bool isSeen, bool isRead , string userId);
    }
}
