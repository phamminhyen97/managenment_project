﻿using AutoMapper;
using log4net;
using Management.Common;
using Management.Entity;
using Management.Service.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Management.Service.Implement
{
    public class CommentService : ICommentService
    {
        private readonly ManagementDB _context;
        private readonly ILog _logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly Mapper _mapper = MapperConfig.MapperConfig.InitializeAutomapper();
        public CommentService(ManagementDB context)
        {
            _context = context;
        }
        public async Task CreateAsync(CommentIssueViewModel dto)
        {
            try
            {
                var entity = _mapper.Map<CommentIssueViewModel, CommentIssue>(dto);
                entity.CreateDate = DateTime.Now;
                //entity.Module.Histories.Add(new HistoryModule
                //{
                //    AssignedFrom = dto.UserId,
                //    CreateDate = DateTime.Now,
                //    IdModule = dto.ModuleId,
                //    Description = $"{dto.UserName} commented in issue",
                //    Status = (int)EnumConstant.History.Comment,
                //});
               
                _context.CommentIssues.Add(entity);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                _logger.Error($"Create comment failed by error[{ex}]");
            }
        }

        public async Task<ResultModel> CreateCommentAsync(CommentIssueViewModel model)
        {
            try
            {
                var entity = _mapper.Map<CommentIssueViewModel, CommentIssue>(model);
                entity.CreateDate = DateTime.Now;
                _context.CommentIssues.Add(entity);
                var users = _context.UserJoinProjects.Where(x => x.Project.Modules.Where(q => q.Id == model.ModuleId).Any() && x.IdUser!=model.UserId).ToList();
                foreach (var item in users)
                {
                    _context.Notifications.Add(new Notification
                    {
                        CreateDate = DateTime.Now,
                        FromUser = model.UserId,
                        ToUser = item.IdUser,
                        Description = $"<strong class='text-primary'>{model.UserName}</strong>" + $" Commented in <strong class='text-primary'>{model.ModuleName}</strong>",
                        TypeId = (int)(EnumConstant.NotificationType.Comment),
                        UpdateDate=DateTime.Now,
                        ModuleId=model.ModuleId
                    });
                }

                await _context.SaveChangesAsync();
                return new ResultModel { Issuccess = true, Result = entity.Id };
            }
            catch (Exception ex)
            {
                _logger.Error($"Create comment failed by error[{ex}]");
                return new ResultModel { Issuccess = false, Result = $"Create comment failed by error[{ex.Message}]" };
            }
        }

        public async Task DeleteAsync(object keyValues)
        {
            try
            {
                var entity = await _context.CommentIssues.FindAsync(keyValues);
                if (entity != null)
                {
                    _context.CommentIssues.Remove(entity);
                    await _context.SaveChangesAsync();
                }
                else
                {
                    _logger.Error($"Comment by Id [{keyValues}] not exists in db!");
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"Delete comment[{keyValues}] failed by error: [{ex}]");
            }
        }

        public async Task<CommentIssueViewModel> FindByIdAsync(object keyValues)
        {
            try
            {
                var entity = await _context.CommentIssues.FindAsync(keyValues);
                if (entity != null)
                {
                    return _mapper.Map<CommentIssue, CommentIssueViewModel>(entity);
                }
                _logger.Error($" Comment[{keyValues}] not exists in db!");
                return null;
            }
            catch (Exception ex)
            {

                _logger.Error($"Get Comment of issue[{keyValues}] failed by error [{ex}]!");
                return null;
            }
        }

        public Task<IEnumerable<CommentIssueViewModel>> GetAll(int? projectId = null, string userId = null)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<CommentIssueViewModel>> GetCommentOfIssue(int issueId)
        {
            try
            {
                return _context.CommentIssues.AsEnumerable().Where(x => x.ModuleId == issueId).Select(x => _mapper.Map<CommentIssue, CommentIssueViewModel>(x));
            }
            catch (Exception ex)
            {
                _logger.Error($"Get Comments of issue[{issueId}] failed by error [{ex}]!");
                return null;
            }
        }

        public async Task<bool> UpdateAsync(CommentIssueViewModel dto)
        {
            try
            {
                var entity = await _context.CommentIssues.FindAsync(dto.Id);
                if (entity != null)
                {
                    entity.UpdateDate = DateTime.Now;
                    entity.Description = dto.Description;
                    await _context.SaveChangesAsync();
                }
                _logger.Error($"Comment by Id[{dto.Id} not exists in db!]");
                return false;
            }
            catch (Exception ex)
            {
                _logger.Error($"Update comment failed by error[{ex}]!");
                return false;
            }
        }
    }
}
