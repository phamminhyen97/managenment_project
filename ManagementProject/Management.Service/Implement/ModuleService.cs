﻿using AutoMapper;
using log4net;
using Management.Common;
using Management.Entity;
using Management.Service.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using static Management.Common.EnumConstant;
using Module = Management.Entity.Module;

namespace Management.Service.Implement
{
    public class ModuleService : IModuleService
    {
        private readonly ManagementDB _context;
        private readonly ILog _logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly Mapper _mapper = MapperConfig.MapperConfig.InitializeAutomapper();

        public ModuleService(ManagementDB context)
        {
            _context = context;
        }
        public async Task CreateAsync(ModuleViewModel dto)
        {
            try
            {
                dto.Name = await GeneratedKey(dto.IdProject);
                var entity = _mapper.Map<ModuleViewModel, Module>(dto);
                entity.Histories = new List<HistoryModule>();
                entity.Histories.Add(new HistoryModule
                {
                    IdModule = entity.Id,
                    Status = (int)History.Create,
                    CreateDate = DateTime.Now,
                    AssignedFrom = entity.CreateBy,
                    Description = $"<a class='text-primary'>{dto.CreateName}</a> created issue <a class='text-primary'>{dto.Name}</a>",
                });
                _context.Modules.Add(entity);
                await _context.SaveChangesAsync();

            }
            catch (Exception ex)
            {

                _logger.ErrorFormat(LoggingEnum.FL0011.GetDescription(), dto.Name, ex);
            }
        }

        public Task DeleteAsync(object keyValues)
        {
            throw new NotImplementedException();
        }

        public async Task<ModuleViewModel> FindByIdAsync(object keyValues)
        {
            try
            {
                var entity = await _context.Modules.FindAsync(keyValues);
                if (entity != null)
                {
                    var tmp = _mapper.Map<Module, ModuleViewModel>(entity);
                    var logged = entity.WorkLogs?.Sum(x => x.TimeLog);
                    if (entity.Module1.Count > 0)
                    {
                        var loggedSubTask = entity.Module1.Select(x => x.WorkLogs.Sum(q => q.TimeLog)).Sum();
                        logged += loggedSubTask;
                    }
                    var remaining = tmp.Estimated - logged;
                    tmp.Remaining = remaining > 0 ? remaining.GetValueOrDefault() : 0;
                    tmp.Logged = logged.GetValueOrDefault();
                    //return _mapper.Map<Module, ModuleViewModel>(entity);
                    return tmp;
                }
                _logger.Error($"Module [{keyValues}] not exists in db!");
                return null;

            }
            catch (Exception ex)
            {
                _logger.ErrorFormat("Get Module failed by error [{0}]", ex);
                return null;
            }
        }

        public async Task<IEnumerable<ModuleViewModel>> GetAll(int? projectId = null, string userId = null)
        {
            try
            {
                var result = _context.Modules.AsEnumerable().Select(x => _mapper.Map<Module, ModuleViewModel>(x));

                if (projectId != null && userId != null)
                {
                    result = result.Where(x => x.IdProject == projectId && x.UserId == userId);
                }
                if (projectId != null)
                {
                    result = result.Where(x => x.IdProject == projectId);
                }
                if (userId != null)
                {
                    result = result.Where(x => x.UserId == userId);
                }

                foreach (var item in result)
                {
                    var logged = item.WorkLogs?.Sum(x => x.TimeLog);
                    if (item.Module1.Count > 0)
                    {
                        var loggedSubTask = item.Module1.Select(x => x.WorkLogs.Sum(q => q.TimeLog)).Sum();
                        logged += loggedSubTask;
                    }
                    var remaining = item.Estimated - logged;
                    item.Remaining = remaining > 0 ? remaining.GetValueOrDefault() : 0;
                    item.Logged = logged.GetValueOrDefault();
                }
                return result;
            }
            catch (Exception ex)
            {
                _logger.ErrorFormat("Get Modules failed by error [{0}]", ex);
                return null;
            }
        }

        public Task<bool> UpdateAsync(ModuleViewModel dto)
        {
            throw new NotImplementedException();
        }

        public async Task<string> GeneratedKey(int? id)
        {
            try
            {
                var entity = await _context.Projects.FindAsync(id);

                var moduleName = entity.Modules.OrderByDescending(x => x.Name)?.FirstOrDefault()?.Name;
                var key = new StringBuilder(entity.Key);
                if (string.IsNullOrEmpty(moduleName))
                {
                    key.Append("-0001");
                }
                else
                {
                    var idx = moduleName.IndexOf('-') + 1;
                    var idKey = int.Parse(moduleName.Substring(idx, moduleName.Length - idx).Replace("0", ""));
                    if (idKey < 9)
                    {
                        key.Append("-000").Append(idKey + 1);
                    }
                    else if (idKey < 99)
                    {
                        key.Append("-00").Append(idKey + 1);
                    }
                    else
                    {
                        key.Append("-0").Append(idKey + 1);
                    }
                }
                return key.ToString();
            }
            catch (Exception ex)
            {
                _logger.ErrorFormat("GeneratedKey error :{0}", ex);
                return string.Empty;
            }
        }

        public async Task<ResultModel> UpdateStatus(string statusFrom, string statusTo, int id, string username, string userId)
        {
            try
            {
                var entity = await _context.Modules.FindAsync(id);
                var des = Helper.GetStatus(statusTo);
                var status = Helper.GetStatus(statusFrom);
                if (entity != null)
                {
                    switch (statusTo)
                    {
                        case "todo-left":
                            entity.Status = (int)EnumConstant.IssueStatus.Todo;
                            break;
                        case "process-mid":
                            entity.Status = (int)EnumConstant.IssueStatus.Progress;
                            break;
                        case "review-mid":
                            entity.Status = (int)EnumConstant.IssueStatus.Review;
                            break;
                        case "done-right":
                            entity.Status = (int)EnumConstant.IssueStatus.Done;
                            break;
                        case "close":
                            entity.Status = (int)EnumConstant.IssueStatus.Close;
                            break;
                        default:
                            entity.Status = (int)EnumConstant.IssueStatus.Close;
                            break;
                    }
                    entity.Histories.Add(new HistoryModule
                    {
                        CreateDate = DateTime.Now,
                        Description = $"<a class='text-primary'>{username}</a>" + $" changed status from <a class='text-primary'>{status}</a> to <a class='text-primary'>{des}</a>",
                        IdModule = id,
                        AssignedFrom = userId,
                        Status = (int)EnumConstant.History.ChangeStatus,
                        UpdateDate = DateTime.Now
                    });

                    //add notification
                    var userJoins = _context.UserJoinProjects.Where(x => x.IdUser != userId && x.Project.Modules.Where(q => q.Id == id).Any()).ToList();
                    foreach (var item in userJoins)
                    {
                        _context.Notifications.Add(new Notification
                        {
                            CreateDate = DateTime.Now,
                            FromUser = userId,
                            ToUser = item.IdUser,
                            Description = $"<strong class='text-primary'>{username}</strong>" + $" Changed status from <strong class='text-danger'>{status} </strong> to <strong class='text-danger'>{des} </strong> in  <strong class='text-primary'>{entity.Name}</strong>",
                            TypeId = (int)(EnumConstant.NotificationType.UpdateIssue),
                            UpdateDate = DateTime.Now,
                            ModuleId = id
                        });
                    }
                    await _context.SaveChangesAsync();
                    return new ResultModel() { Issuccess = true };
                }
                return new ResultModel() { Issuccess = false, Result = "Issue not exists in db!" };
            }
            catch (Exception ex)
            {
                _logger.ErrorFormat("Update status Issue error :{0}", ex);
                return new ResultModel() { Issuccess = false, Result = ex.Message };
            }
        }

        public async Task<ResultModel> UpdateDetail(ModuleUpdateViewModel model)
        {
            try
            {
                var entity = await _context.Modules.FindAsync(model.IdModule);
                var description = string.Empty;
                if (entity != null)
                {
                    if (model.IssueStatus != null)
                    {
                        description = $"<a class='text-primary'>{ model.UserName}</a> changed status from <a class='text-primary'>{Extensions.GetValue(entity.Status.Value, "status")}</a> to <a class='text-primary'>{Extensions.GetValue(model.IssueStatus.Value, "status")}</a> of issue <a class='text-primary'>{model.ModuleName}</a>  ";
                        entity.Status = model.IssueStatus;
                    }
                    if (model.IssueType != null)
                    {
                        description = $"<a class='text-primary'>{ model.UserName}</a> changed issue type from <a class='text-primary'>{Extensions.GetValue(entity.IssueType, "issue")}</a> to <a class='text-primary'>{Extensions.GetValue(model.IssueType.Value, "issue")}</a> of issue <a class='text-primary'>{model.ModuleName}</a> ";
                        entity.IssueType = model.IssueType.Value;
                    }
                    if (model.PriorityType != null)
                    {
                        description = $"<a class='text-primary'>{ model.UserName}</a> changed Priority from <a class='text-primary'>{Extensions.GetValue(entity.PriorityType, "priority")}</a> to <a class='text-primary'>{Extensions.GetValue(model.PriorityType.Value, "priority")}</a> of issue <a class='text-primary'>{model.ModuleName}</a>";
                        entity.PriorityType = model.PriorityType.Value;
                    }
                    if (!string.IsNullOrEmpty(model.AssigneeFrom))
                    {
                        description = $"<a class='text-primary'>{ model.UserName}</a> changed Assignee from <a class='text-primary'>{model.AssigneeFrom}</a> to <a class='text-primary'>{model.AssigneeTo}</a> of issue <a class='text-primary'>{model.ModuleName}</a>";
                        entity.UserId = model.AssigneeId;
                    }

                    if (!string.IsNullOrEmpty(model.ReporterFrom))
                    {
                        description = $"<a class='text-primary'>{ model.UserName}</a> changed Assignee from <a class='text-primary'>{model.ReporterFrom}</a> to <a class='text-primary'>{model.ReporterTo}</a> of issue <a class='text-primary'>{model.ModuleName}</a>";
                        entity.Reporter = model.ReporterId;
                    }
                    if (model.Point != null)
                    {
                        description = $"<a class='text-primary'>{ model.UserName}</a> changed Time from <a class='text-primary'>{entity.Point}</a> to <a class='text-primary'>{model.Point}</a> of issue <a class='text-primary'>{model.ModuleName}</a>";
                        entity.Point = model.Point;
                    }
                    if (!string.IsNullOrEmpty(model.Description))
                    {
                        description = $"<a class='text-primary'>{ model.UserName}</a> changed Description of issue <a class='text-primary'>{model.ModuleName}</a>";
                        entity.Description = model.Description;
                    }
                    if (entity.Histories != null)
                    {
                        entity.Histories.Add(new HistoryModule
                        {
                            AssignedFrom = model.UserId,
                            CreateDate = DateTime.Now,
                            Description = description,
                            IdModule = entity.Id,
                            UpdateDate = DateTime.Now,
                            Status = (int)History.Update,
                        });
                    }
                    await _context.SaveChangesAsync();
                    return new ResultModel() { Issuccess = true, Result = "Update Issue successfully!" };
                }
                _logger.Error("Update Detail issue error :[Issue not exists in db!]");
                return new ResultModel() { Issuccess = false, Result = "Issue not exists in db!" };
            }
            catch (Exception ex)
            {
                _logger.ErrorFormat("Update Detail issue error :{0}", ex);
                return new ResultModel() { Issuccess = false, Result = "Issue not exists in db!" };
            }
        }

        public async Task<ResultModel> CreateSubTask(SubTaskViewModel model)
        {
            try
            {
                model.Name = await GeneratedKey(model.ProjectId);
                var entity = _mapper.Map<SubTaskViewModel, Module>(model);
                entity.Histories = new List<HistoryModule>();
                entity.Histories.Add(new HistoryModule
                {
                    IdModule = entity.IdModuleParent.Value,
                    Status = (int)History.CreateSubtask,
                    CreateDate = DateTime.Now,
                    AssignedFrom = entity.CreateBy,
                    Description = $"<a class='text-primary'>{model.UserName}</a> created <a class='text-danger'> child-issue </a> <a class='text-primary'>{model.Name}</a>",
                });
                _context.Modules.Add(entity);
                await _context.SaveChangesAsync();
                return new ResultModel { Issuccess = true, Result = new { Id = entity.Id, Name = entity.Name } };
            }
            catch (Exception ex)
            {
                _logger.Error($"Create subtask[{model.Name}] failed by error:[{ex.Message}]");
                return new ResultModel { Issuccess = false, Result = $"Create subtask[{model.Name}] failed by error:[{ex.Message}]" };
            }
        }

        public async Task<IEnumerable<ModuleViewModel>> GetModuleByFilter(string userId)
        {
            try
            {
                var result = _context.Modules.Where(x => x.Project.UserJoinProjects.Where(q => q.IdUser == userId).Any() || x.Project.IdManager == userId).AsEnumerable().Select(x => _mapper.Map<Module, ModuleViewModel>(x));
                return result;
            }
            catch (Exception ex)
            {

                _logger.Error($"Get All Module by user failed by error [{ex.Message}]");
                return null;
            }
        }
    }
}
