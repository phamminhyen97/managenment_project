﻿using AutoMapper;
using log4net;
using Management.Entity;
using Management.Service.ViewModel;
using Microsoft.Practices.ObjectBuilder2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Service.Implement
{
    public class NotificationService : INotificationService
    {
        private readonly ManagementDB _context;
        private readonly ILog _logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly Mapper _mapper = MapperConfig.MapperConfig.InitializeAutomapper();
        public NotificationService(ManagementDB context)
        {
            _context = context;
        }
        public Task CreateAsync(NotificationViewModel dto)
        {
            throw new NotImplementedException();
        }

        public Task DeleteAsync(object keyValues)
        {
            throw new NotImplementedException();
        }

        public Task<NotificationViewModel> FindByIdAsync(object keyValues)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<NotificationViewModel>> GetAll(int? projectId = null, string userId = null)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<NotificationViewModel>> GetNotificationByFilter(bool isSeen, bool isRead, string userId)
        {
            try
            { 
                var notifis = _context.Notifications.AsEnumerable().Where(x=>x.ToUser==userId).OrderByDescending(x=>x.CreateDate).Select(x => _mapper.Map<Notification, NotificationViewModel>(x));
                if (isSeen)
                {
                    notifis = notifis.Where(x => x.IsSeen == false).Take(5);
                }
                if (isRead)
                {
                    notifis = notifis.Where(x => x.IsRead == false);
                }
                return notifis;
            }
            catch (Exception ex)
            {

                _logger.Error($"Get Notifications failed by error [{ex.Message}]");
                return null;
            }
        }

        public Task<bool> UpdateAsync(NotificationViewModel dto)
        {
            throw new NotImplementedException();
        }
    }
}
