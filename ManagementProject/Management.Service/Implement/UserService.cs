﻿using AutoMapper;
using AutoMapper.Mappers;
using log4net;
using Management.Entity;
using Management.Service.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Service.Implement
{
    public class UserService : IUserService
    {
        private readonly ManagementDB _context;
        private readonly ILog _logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly Mapper _mapper = MapperConfig.MapperConfig.InitializeAutomapper();
        public UserService(ManagementDB context)
        {
            _context = context;
        }
        public Task CreateAsync(UserViewModel dto)
        {
            throw new NotImplementedException();
        }

        public Task DeleteAsync(object keyValues)
        {
            throw new NotImplementedException();
        }

        public async Task<UserViewModel> FindByIdAsync(object keyValues)
        {
            try
            {
                var entity =  _context.Users.Find(keyValues);
                if (entity != null)
                {
                    return _mapper.Map<ApplicationUser, UserViewModel>(entity);
                }
                _logger.Error($"User by id [{keyValues}] not exists in db!");
                return null;
            }
            catch (Exception ex)
            {

                return null;
            }
        }

        public Task<IEnumerable<UserViewModel>> GetAll(int? projectId, string userId = null)
        {
            try
            {
                return null;
            }
            catch (Exception ex)
            {

                return null;
            }
        }

        public IEnumerable<UserViewModel> GetUsers(int? projectId)
        {
            try
            {
                if (projectId != null)
                {
                    return _context.Users.AsEnumerable().Where(x => x.UserJoinProjects.Where(q => q.IdProject == projectId).Any()).Select(x => _mapper.Map<ApplicationUser, UserViewModel>(x));
                }
                return _context.Users.AsEnumerable().Select(x => _mapper.Map<ApplicationUser, UserViewModel>(x));
            }
            catch (Exception ex)
            {
                _logger.ErrorFormat("Get User error[{0}]", ex);
                return null;
            }
        }

        public Task<bool> UpdateAsync(UserViewModel dto)
        {
            throw new NotImplementedException();
        }
    }
}
