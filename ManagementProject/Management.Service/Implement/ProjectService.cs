﻿using AutoMapper;
using log4net;
using Management.Common;
using Management.Entity;
using Management.Service.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using static Management.Common.EnumConstant;

namespace Management.Service.Implement
{
    public class ProjectService : IProjectService
    {

        private readonly ManagementDB _context;
        private readonly ILog _logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly Mapper _mapper = MapperConfig.MapperConfig.InitializeAutomapper();
        public ProjectService(ManagementDB context)
        {
            _context = context;
        }
        public async Task CreateAsync(ProjectViewModel dto)
        {
            try
            {
                dto.UserJoinProjects = new List<UserJoinProjectViewModel>();
                dto.UserJoinProjects.Add(new UserJoinProjectViewModel
                {
                    IdProject = dto.Id,
                    IdUser = dto.IdManager,
                    JoinedDay = DateTime.Now,
                    Description = "Add leader to project",
                    IdRole = (int)RoleProject.Lead
                });
                var entity = _mapper.Map<ProjectViewModel, Project>(dto);
                entity.CreateDay = DateTime.Now;
                entity.Status = (int)EnumConstant.ProjectStatus.InProgress;
                _context.Projects.Add(entity);
                await _context.SaveChangesAsync();
                _logger.InfoFormat(LoggingEnum.SS0001.GetDescription(), entity.Id);
            }
            catch (Exception ex)
            {
                _logger.ErrorFormat(LoggingEnum.FL0001.GetDescription(), dto.Name, ex);
            }
        }

        public async Task DeleteAsync(object keyValues)
        {
            try
            {
                var entity = await _context.Projects.FindAsync(keyValues);
                if (entity != null)
                {
                    _context.Projects.Remove(entity);
                    await _context.SaveChangesAsync();
                    _logger.InfoFormat(LoggingEnum.SS0003.GetDescription(), keyValues);
                }

                _logger.ErrorFormat(LoggingEnum.FL0003.GetDescription(), keyValues, "Project not existis in db!");
            }
            catch (Exception ex)
            {
                _logger.ErrorFormat(LoggingEnum.FL0003.GetDescription(), keyValues, ex);
            }
        }

        public async Task<ProjectViewModel> FindByIdAsync(object keyValues)
        {
            try
            {
                var entity = await _context.Projects.FindAsync(keyValues);
                if (entity != null)
                {
                    return _mapper.Map<Project, ProjectViewModel>(entity);
                }

                _logger.Error("Project not existis in db!");
                return null;
            }
            catch (Exception ex)
            {
                _logger.Error($"Get Project error by [{ex}] ");
                return null;
            }
        }

        public async Task<IEnumerable<ProjectViewModel>> GetAll(int? projectId = null, string userId = null)
        {
            try
            {
                var result = _context.Projects.AsEnumerable().Select(x => _mapper.Map<Project, ProjectViewModel>(x));
                if (projectId != null && userId != null)
                {
                    result = result.Where(x => x.Id == projectId && x.UserJoinProjects.Where(q => q.IdUser == userId).Any());
                }
                if (projectId != null)
                {
                    result = result.Where(x => x.Id == projectId);
                }
                if (userId != null)
                {
                    result = result.Where(x => x.UserJoinProjects.Where(q => q.IdUser == userId).Any());
                }
                result = result.ToList();
                foreach (var item in result)
                {
                    foreach (var task in item.Modules)
                    {
                        var logged = task.WorkLogs?.Sum(x => x.TimeLog);
                        if (task.Module1.Count > 0)
                        {
                            var loggedSubTask = task.Module1.Select(x => x.WorkLogs.Sum(q => q.TimeLog)).Sum();
                            logged += loggedSubTask;
                        }
                        var remaining = task.Estimated - logged;
                        task.Remaining = remaining > 0 ? remaining.GetValueOrDefault() : 0;
                        task.Logged = logged.GetValueOrDefault();
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                _logger.Error($"Get Project error by [{ex}] ");
                return null;
            }
        }

        public async Task<bool> UpdateAsync(ProjectViewModel dto)
        {
            try
            {
                var entity = await _context.Projects.FindAsync(dto.Id);
                if (entity != null)
                {

                    //db.Configuration.LazyLoadingEnabled = false;
                    //var msguser = Mapper.Map<BAPUSER>(message);
                    //var dbuser = db.BAPUSER.FirstOrDefault(w => w.BAPUSERID == 1111);
                    //Mapper.Map(msguser, dbuser);
                    ////  db.Entry(userx).State = EntityState.Modified;

                    //db.SaveChanges();
                    entity.Description = dto.Description;
                    entity.Name = dto.Name;
                    entity.UpdateDate = DateTime.Now;
                    entity.IdManager = dto.IdManager;
                    entity.StarDay = dto.StarDay;
                    entity.FinishDay = dto.FinishDay;
                    entity.Status = dto.Status;
                    //var newEntity = _mapper.Map<ProjectViewModel, Project>(dto);
                    //_context.Entry(entity).CurrentValues.SetValues(newEntity);
                    await _context.SaveChangesAsync();
                    return true;
                }

                _logger.Error("Project not existis in db!");
                return false;
            }
            catch (Exception ex)
            {
                _logger.Error($"Get Project error by [{ex}] ");
                return false;
            }
        }

        public async Task<ResultModel> UpdateLeader(int projectId, string userId)
        {
            try
            {
                var entity = await _context.Projects.FindAsync(projectId);
                if (entity != null)
                {
                    entity.IdManager = userId;
                    await _context.SaveChangesAsync();
                    return new ResultModel { Issuccess = true };
                }
                return new ResultModel { Issuccess = false, Result = $"Project by Id[{projectId}] not exists in db!" };
            }
            catch (Exception ex)
            {
                _logger.Error($"Get Project error by [{ex}] ");
                return new ResultModel { Issuccess = false, Result = $"Update leader project failed by error[{ex}]" };
            }
        }

        public async Task<ResultModel> UpsertUserToProject(int projectId, string userId)
        {
            try
            {
                var entity = await _context.UserJoinProjects.FirstOrDefaultAsync(x => x.IdProject == projectId && x.IdUser == userId);
                if (entity != null)
                {
                    _context.UserJoinProjects.Remove(entity);
                }
                else
                {
                    _context.UserJoinProjects.Add(new UserJoinProject
                    {
                        Description = $"Add user[{userId}] to project[{projectId}]",
                        IdProject = projectId,
                        IdRole = 3,
                        JoinedDay = DateTime.Now,
                        IdUser = userId
                    });
                }
                await _context.SaveChangesAsync();
                return new ResultModel { Issuccess = true };
            }
            catch (Exception ex)
            {
                _logger.Error($"Upsert user error by [{ex}] ");
                return new ResultModel { Issuccess = false, Result = $"Upsert user failed by error[{ex}]" };
            }
        }
    }
}
