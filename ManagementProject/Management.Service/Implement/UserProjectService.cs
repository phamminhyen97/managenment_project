﻿using Management.Service.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Service.Implement
{
    public class UserProjectService : IUserProjectService
    {
        public Task CreateAsync(UserJoinProjectViewModel dto)
        {
            throw new NotImplementedException();
        }

        public Task DeleteAsync(object keyValues)
        {
            throw new NotImplementedException();
        }

        public Task<UserJoinProjectViewModel> FindByIdAsync(object keyValues)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<UserJoinProjectViewModel>> GetAll(int? projectId, string userId = null)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateAsync(UserJoinProjectViewModel dto)
        {
            throw new NotImplementedException();
        }
    }
}
