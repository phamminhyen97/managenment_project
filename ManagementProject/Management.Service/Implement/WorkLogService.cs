﻿using AutoMapper;
using log4net;
using Management.Entity;
using Management.Service.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Service.Implement
{
    public class WorkLogService : IWorkLogService
    {
        private readonly ManagementDB _context;
        private readonly ILog _logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly Mapper _mapper = MapperConfig.MapperConfig.InitializeAutomapper();
        public WorkLogService(ManagementDB context)
        {
            _context = context;
        }
        public async Task CreateAsync(WorkLogViewModel dto)
        {
            try
            {
                dto.CreateDate = DateTime.Now;
                var entity = _mapper.Map<WorkLogViewModel, WorkLog>(dto);
                _context.WorkLogs.Add(entity);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                _logger.Error($"Add logwork failed by error [{ex.Message}]");
            }
        }

        public Task DeleteAsync(object keyValues)
        {
            throw new NotImplementedException();
        }

        public Task<WorkLogViewModel> FindByIdAsync(object keyValues)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<WorkLogViewModel>> GetAll(int? projectId = null, string userId = null)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateAsync(WorkLogViewModel dto)
        {
            throw new NotImplementedException();
        }
    }
}
