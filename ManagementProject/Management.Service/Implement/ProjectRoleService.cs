﻿using Management.Service.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Service.Implement
{
    public class ProjectRoleService : IProjectRoleService
    {
        public Task CreateAsync(ProjectRoleViewModel dto)
        {
            throw new NotImplementedException();
        }

        public Task DeleteAsync(object keyValues)
        {
            throw new NotImplementedException();
        }

        public Task<ProjectRoleViewModel> FindByIdAsync(object keyValues)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<ProjectRoleViewModel>> GetAll(int? projectId, string userId = null)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateAsync(ProjectRoleViewModel dto)
        {
            throw new NotImplementedException();
        }
    }
}
