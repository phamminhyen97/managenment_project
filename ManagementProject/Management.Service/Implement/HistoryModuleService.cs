﻿using Management.Service.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Service.Implement
{
    public class HistoryModuleService : IHistoryModuleService
    {
        public Task CreateAsync(HistoryModuleViewModel dto)
        {
            throw new NotImplementedException();
        }

        public Task DeleteAsync(object keyValues)
        {
            throw new NotImplementedException();
        }

        public Task<HistoryModuleViewModel> FindByIdAsync(object keyValues)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<HistoryModuleViewModel>> GetAll(int? projectId=null, string userId = null)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateAsync(HistoryModuleViewModel dto)
        {
            throw new NotImplementedException();
        }
    }
}
