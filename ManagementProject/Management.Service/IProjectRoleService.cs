﻿using Management.Entity;
using Management.Service.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Service
{
    public interface IProjectRoleService : IBaseService<ProjectRole, ProjectRoleViewModel>
    {
    }
}
