namespace Management.Entity
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using Microsoft.AspNet.Identity.EntityFramework;

    public partial class ManagementDB : IdentityDbContext<ApplicationUser>
    {
        public ManagementDB()
            : base("name=ManagementDB")
        {
        }

        public virtual DbSet<Module> Modules { get; set; }
        public virtual DbSet<Project> Projects { get; set; }
        public virtual DbSet<ProjectRole> ProjectRoles { get; set; }
        public virtual DbSet<UserJoinProject> UserJoinProjects { get; set; }
        public virtual DbSet<CommentIssue> CommentIssues { get; set; }
        public virtual DbSet<WorkLog> WorkLogs { get; set; }
        public virtual DbSet<HistoryModule> HistoryModules { get; set; }

        public virtual DbSet<Notification> Notifications { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            //modelBuilder.Entity<Module>()
            //    .HasMany(e => e.Module1)
            //    .WithOptional(e => e.Module2)
            //    .HasForeignKey(e => e.IdModuleParent);

            //modelBuilder.Entity<Project>()
            //    .Property(e => e.IdManager)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Project>()
            //    .HasMany(e => e.Modules)
            //    .WithOptional(e => e.Project)
            //    .HasForeignKey(e => e.IdProject);

            //modelBuilder.Entity<Project>()
            //    .HasMany(e => e.UserJoinProjects)
            //    .WithOptional(e => e.Project)
            //    .HasForeignKey(e => e.IdProject);

            //modelBuilder.Entity<ProjectRole>()
            //    .HasMany(e => e.UserJoinProjects)
            //    .WithOptional(e => e.ProjectRole)
            //    .HasForeignKey(e => e.IdRole);
        }
        public static ManagementDB Create()
        {
            return new ManagementDB();
        }
    }
}
