namespace Management.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Project")]
    public partial class Project
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Project()
        {
            Modules = new HashSet<Module>();
            UserJoinProjects = new HashSet<UserJoinProject>();
        }

        public int Id { get; set; }

        [StringLength(500)]
        public string Name { get; set; }

        
        [StringLength(4)]
        public string Key { get; set; }

        [StringLength(500)]
        public string Description { get; set; }

        [StringLength(128)]
        public string IdManager { get; set; }

        public DateTime? StarDay { get; set; }

        public DateTime? FinishDay { get; set; }

        public DateTime? CreateDay { get; set; }

        public DateTime? UpdateDate { get; set; }

        public int? Status { get; set; }

        public string Avatar { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Module> Modules { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UserJoinProject> UserJoinProjects { get; set; }
    }
}
