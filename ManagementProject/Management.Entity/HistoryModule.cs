﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Entity
{
    public class HistoryModule
    {
        public long Id { get; set; }

        [ForeignKey("Module")]
        public int IdModule { get; set; }

        [ForeignKey("FromUser")]
        public string AssignedFrom { get; set; }

        [ForeignKey("ToUser")]
        public string AssignedTo { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string Description { get; set; }
        public int Status { get; set; }
        public virtual Module Module { get; set; }

        public virtual ApplicationUser FromUser { get; set; }
        public virtual ApplicationUser ToUser { get; set; }
    }
}
