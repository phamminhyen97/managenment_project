﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Entity
{
    public class Notification
    {
        public long Id { get; set; }
        public int TypeId { get; set; }
        public string Description { get; set; }
        public string FromUser { get; set; }
        public string ToUser { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }

        public int ModuleId { get; set; }

        [DefaultValue(false)]
        public bool IsSeen { get; set; }

        [DefaultValue(false)]
        public bool IsRead { get; set; }

        [ForeignKey("TypeId")]
        public virtual Type Type { get; set; }

        [ForeignKey("FromUser")]
        public virtual ApplicationUser FromSend { get; set; }

        [ForeignKey("ToUser")]
        public virtual ApplicationUser SendTo { get; set; }
    }
}
