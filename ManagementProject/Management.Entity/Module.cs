namespace Management.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Module")]
    public partial class Module
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Module()
        {
            Module1 = new HashSet<Module>();
            Histories = new HashSet<HistoryModule>();
        }
        public int Id { get; set; }

        [ForeignKey("Project")]
        public int? IdProject { get; set; }

        public string Name { get; set; }

        public string Summary { get; set; }

        public string Description { get; set; }

        public DateTime? StartDay { get; set; }

        public DateTime? FinishDay { get; set; }

        public int? Point { get; set; }

        [ForeignKey("ModuleParent")]
        public int? IdModuleParent { get; set; }

        public int? Status { get; set; }

        public string UserId { get; set; }

        public string CreateBy { get; set; }

        public int IssueType { get; set; }

        public string Assignee { get; set; }
        public int PriorityType { get; set; }
        public string Reporter { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Module> Module1 { get; set; }

        public virtual ICollection<HistoryModule> Histories { get; set; }

        public virtual Module ModuleParent { get; set; }

        public virtual Project Project { get; set; }


        [ForeignKey("UserId")]
        public virtual ApplicationUser AssignesUser { get; set; }

        [ForeignKey("Reporter")]
        public virtual ApplicationUser ReporterUser { get; set; }

        public virtual ICollection<CommentIssue> Comments { get; set; }
        public virtual ICollection<WorkLog> WorkLogs { get; set; }
    }
}
