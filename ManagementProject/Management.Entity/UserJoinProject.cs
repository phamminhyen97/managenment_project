namespace Management.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("UserJoinProject")]
    public partial class UserJoinProject
    {
        public UserJoinProject()
        {
        }
        public int Id { get; set; }

        [StringLength(128)]
        [ForeignKey("User")]
        public string IdUser { get; set; }

        [ForeignKey("Project")]
        public int IdProject { get; set; }

        public DateTime? JoinedDay { get; set; }

        public string Description { get; set; }

        [ForeignKey("ProjectRole")]
        public int? IdRole { get; set; }

        public virtual Project Project { get; set; }

        public virtual ProjectRole ProjectRole { get; set; }

        public virtual ApplicationUser User { get; set; }

    }
}
