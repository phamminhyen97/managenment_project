﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Management.Entity
{
    public class ApplicationUser : IdentityUser
    {

        public DateTime? CreateDate { get; set; }

        public bool? Sex { get; set; }

        public DateTime? DateOfBirth { get; set; }

        [Column(TypeName = "ntext")]
        public string Avatar { get; set; }

        [StringLength(100)]
        public string Name { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Phone { get; set; }

        public virtual ICollection<UserJoinProject> UserJoinProjects { get; set; }

        public virtual ICollection<CommentIssue> CommentIssues { get; set; }

        public virtual ICollection<WorkLog> WorkLogs { get; set; }

        [InverseProperty("AssignesUser")]
        public virtual ICollection<Module> AssignesUsers { get; set; }

        [InverseProperty("ReporterUser")]
        public virtual ICollection<Module> ReporterUsers { get; set; }


        [InverseProperty("FromSend")]
        public virtual ICollection<Notification> FromSends { get; set; }

        [InverseProperty("SendTo")]
        public virtual ICollection<Notification> SendTos { get; set; }

        [InverseProperty("FromUser")]
        public virtual ICollection<HistoryModule> FromUsers { get; set; }

        [InverseProperty("ToUser")]
        public virtual ICollection<HistoryModule> ToUsers { get; set; }
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }
}
