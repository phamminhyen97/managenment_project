namespace Management.Entity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_field_Summary_tb_Module : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Module", "Summary", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Module", "Summary");
        }
    }
}
