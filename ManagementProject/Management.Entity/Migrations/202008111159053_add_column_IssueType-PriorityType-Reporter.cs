namespace Management.Entity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_column_IssueTypePriorityTypeReporter : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Module", "IsssueType", c => c.Int(nullable: false));
            AddColumn("dbo.Module", "Assignee", c => c.String());
            AddColumn("dbo.Module", "PriorityType", c => c.Int(nullable: false));
            AddColumn("dbo.Module", "Reporter", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Module", "Reporter");
            DropColumn("dbo.Module", "PriorityType");
            DropColumn("dbo.Module", "Assignee");
            DropColumn("dbo.Module", "IsssueType");
        }
    }
}
