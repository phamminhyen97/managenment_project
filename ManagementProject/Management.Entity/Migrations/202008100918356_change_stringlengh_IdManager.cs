namespace Management.Entity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class change_stringlengh_IdManager : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Project", "IdManager", c => c.String(maxLength: 128));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Project", "IdManager", c => c.String(maxLength: 32));
        }
    }
}
