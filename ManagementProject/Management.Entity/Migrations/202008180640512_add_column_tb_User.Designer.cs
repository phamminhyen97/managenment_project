// <auto-generated />
namespace Management.Entity.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class add_column_tb_User : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(add_column_tb_User));
        
        string IMigrationMetadata.Id
        {
            get { return "202008180640512_add_column_tb_User"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
