namespace Management.Entity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_column_Key_table_Project : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Project", "Key", c => c.String(maxLength: 4));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Project", "Key");
        }
    }
}
