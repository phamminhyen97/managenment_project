namespace Management.Entity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_tbWorkLog : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.WorkLogs",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        AuthorId = c.String(maxLength: 128),
                        CreateDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(nullable: false),
                        Description = c.String(),
                        TimeLog = c.Int(nullable: false),
                        ModuleId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.AuthorId)
                .ForeignKey("dbo.Module", t => t.ModuleId, cascadeDelete: true)
                .Index(t => t.AuthorId)
                .Index(t => t.ModuleId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.WorkLogs", "ModuleId", "dbo.Module");
            DropForeignKey("dbo.WorkLogs", "AuthorId", "dbo.AspNetUsers");
            DropIndex("dbo.WorkLogs", new[] { "ModuleId" });
            DropIndex("dbo.WorkLogs", new[] { "AuthorId" });
            DropTable("dbo.WorkLogs");
        }
    }
}
