namespace Management.Entity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_column_IsSeen_IsRead_tb_Notification : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Notifications", "IsSeen", c => c.Boolean(nullable: false));
            AddColumn("dbo.Notifications", "IsRead", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Notifications", "IsRead");
            DropColumn("dbo.Notifications", "IsSeen");
        }
    }
}
