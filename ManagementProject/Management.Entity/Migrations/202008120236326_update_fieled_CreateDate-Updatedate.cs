namespace Management.Entity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class update_fieled_CreateDateUpdatedate : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.HistoryModules", "CreateDate", c => c.DateTime());
            AlterColumn("dbo.HistoryModules", "UpdateDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.HistoryModules", "UpdateDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.HistoryModules", "CreateDate", c => c.DateTime(nullable: false));
        }
    }
}
