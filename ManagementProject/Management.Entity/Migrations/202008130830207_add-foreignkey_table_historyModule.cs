namespace Management.Entity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addforeignkey_table_historyModule : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.HistoryModules", "AssignedFrom", c => c.String(maxLength: 128));
            AlterColumn("dbo.HistoryModules", "AssignedTo", c => c.String(maxLength: 128));
            CreateIndex("dbo.HistoryModules", "AssignedFrom");
            CreateIndex("dbo.HistoryModules", "AssignedTo");
            AddForeignKey("dbo.HistoryModules", "AssignedFrom", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.HistoryModules", "AssignedTo", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.HistoryModules", "AssignedTo", "dbo.AspNetUsers");
            DropForeignKey("dbo.HistoryModules", "AssignedFrom", "dbo.AspNetUsers");
            DropIndex("dbo.HistoryModules", new[] { "AssignedTo" });
            DropIndex("dbo.HistoryModules", new[] { "AssignedFrom" });
            AlterColumn("dbo.HistoryModules", "AssignedTo", c => c.String());
            AlterColumn("dbo.HistoryModules", "AssignedFrom", c => c.String());
        }
    }
}
