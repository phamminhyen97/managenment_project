namespace Management.Entity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class update_filedIssueType : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Module", "IssueType", c => c.Int(nullable: false));
            DropColumn("dbo.Module", "IsssueType");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Module", "IsssueType", c => c.Int(nullable: false));
            DropColumn("dbo.Module", "IssueType");
        }
    }
}
