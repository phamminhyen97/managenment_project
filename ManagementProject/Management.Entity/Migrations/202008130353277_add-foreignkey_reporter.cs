namespace Management.Entity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addforeignkey_reporter : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Module", "Reporter", c => c.String(maxLength: 128));
            CreateIndex("dbo.Module", "Reporter");
            AddForeignKey("dbo.Module", "Reporter", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Module", "Reporter", "dbo.AspNetUsers");
            DropIndex("dbo.Module", new[] { "Reporter" });
            AlterColumn("dbo.Module", "Reporter", c => c.String());
        }
    }
}
