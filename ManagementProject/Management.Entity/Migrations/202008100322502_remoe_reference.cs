namespace Management.Entity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class remoe_reference : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.HistoryModules", "UserJoinProject_Id", "dbo.UserJoinProject");
            DropIndex("dbo.HistoryModules", new[] { "UserJoinProject_Id" });
            DropColumn("dbo.HistoryModules", "UserJoinProject_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.HistoryModules", "UserJoinProject_Id", c => c.Int());
            CreateIndex("dbo.HistoryModules", "UserJoinProject_Id");
            AddForeignKey("dbo.HistoryModules", "UserJoinProject_Id", "dbo.UserJoinProject", "Id");
        }
    }
}
