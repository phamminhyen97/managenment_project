namespace Management.Entity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_Avatar_ForProject : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Project", "Avatar", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Project", "Avatar");
        }
    }
}
