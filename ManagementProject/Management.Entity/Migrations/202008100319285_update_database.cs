namespace Management.Entity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class update_database : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.HistoryModules",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        IdModule = c.Int(nullable: false),
                        AssignedFrom = c.String(),
                        AssignedTo = c.String(),
                        CreateDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(nullable: false),
                        Description = c.String(),
                        Status = c.Int(nullable: false),
                        UserJoinProject_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Module", t => t.IdModule, cascadeDelete: true)
                .ForeignKey("dbo.UserJoinProject", t => t.UserJoinProject_Id)
                .Index(t => t.IdModule)
                .Index(t => t.UserJoinProject_Id);
            
            AddColumn("dbo.Module", "UserId", c => c.String());
            AddColumn("dbo.Module", "CreateBy", c => c.String());
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.HistoryModules", "UserJoinProject_Id", "dbo.UserJoinProject");
            DropForeignKey("dbo.HistoryModules", "IdModule", "dbo.Module");
            DropIndex("dbo.HistoryModules", new[] { "UserJoinProject_Id" });
            DropIndex("dbo.HistoryModules", new[] { "IdModule" });
            DropColumn("dbo.Module", "CreateBy");
            DropColumn("dbo.Module", "UserId");
            DropTable("dbo.HistoryModules");
        }
    }
}
