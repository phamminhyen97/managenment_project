namespace Management.Entity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_tb_Nitification : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Notifications",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        TypeId = c.Int(nullable: false),
                        Description = c.String(),
                        FromUser = c.String(maxLength: 128),
                        ToUser = c.String(maxLength: 128),
                        CreateDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Types", t => t.TypeId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.FromUser)
                .ForeignKey("dbo.AspNetUsers", t => t.ToUser)
                .Index(t => t.TypeId)
                .Index(t => t.FromUser)
                .Index(t => t.ToUser);
            
            CreateTable(
                "dbo.Types",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Notifications", "ToUser", "dbo.AspNetUsers");
            DropForeignKey("dbo.Notifications", "FromUser", "dbo.AspNetUsers");
            DropForeignKey("dbo.Notifications", "TypeId", "dbo.Types");
            DropIndex("dbo.Notifications", new[] { "ToUser" });
            DropIndex("dbo.Notifications", new[] { "FromUser" });
            DropIndex("dbo.Notifications", new[] { "TypeId" });
            DropTable("dbo.Types");
            DropTable("dbo.Notifications");
        }
    }
}
