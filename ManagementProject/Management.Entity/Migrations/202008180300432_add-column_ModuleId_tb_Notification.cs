namespace Management.Entity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addcolumn_ModuleId_tb_Notification : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Notifications", "ModuleId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Notifications", "ModuleId");
        }
    }
}
