namespace Management.Entity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateForeginKey : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Module", "UserId", c => c.String(maxLength: 128));
            CreateIndex("dbo.Module", "UserId");
            AddForeignKey("dbo.Module", "UserId", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Module", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.Module", new[] { "UserId" });
            AlterColumn("dbo.Module", "UserId", c => c.String());
        }
    }
}
