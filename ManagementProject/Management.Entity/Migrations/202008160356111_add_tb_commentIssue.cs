namespace Management.Entity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_tb_commentIssue : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CommentIssues",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        UserId = c.String(maxLength: 128),
                        CreateDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(),
                        Description = c.String(),
                        ModuleId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Module", t => t.ModuleId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.ModuleId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CommentIssues", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.CommentIssues", "ModuleId", "dbo.Module");
            DropIndex("dbo.CommentIssues", new[] { "ModuleId" });
            DropIndex("dbo.CommentIssues", new[] { "UserId" });
            DropTable("dbo.CommentIssues");
        }
    }
}
